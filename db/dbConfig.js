let Sequelize = require("sequelize");
let config = require("../config.json")
let sequelize = new Sequelize(config.db_name, config.username, config.password, {
    host: config.host,
    dialect: config.dialect,
    operatorsAliases: config.alias,

    pool: {
        max: config.max_conn,
        min: config.min_conn,
        idle: config.idle
    },

});

sequelize.sync();

module.exports = sequelize;