let errorHandler = require("../lib/util")

const handleErrorResponse = (res, err, next) =>{
        res.status(400).send(errorHandler(err));            
};
const handleSuccessResponse = (req, res, next, data) =>{
    res.json(data)
}

module.exports= {
    handleErrorResponse,
    handleSuccessResponse
}