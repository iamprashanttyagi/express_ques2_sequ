let User = require('../model/Users')
let valid_data = require('../providers/UserProvider.js');
let BaseAPIController = require('./BaseAPIController')
let md5 = require('md5');

exports.register = function (req, res) {

    valid_data(req.body).then(data => {
        User.create({
            username: data.username,
            firstname: data.firstname,
            lastname: data.lastname,
            pass: md5(data.pass),
            email: data.email,
        }).then(user => {
            BaseAPIController.handleSuccessResponse(req, res, null, user)
        }).catch(error => {
            BaseAPIController.handleErrorResponse(res, error.errors[0])
        });
    }).catch(error => {
        BaseAPIController.handleErrorResponse(res, error)
    });
}