let Sequelize = require("sequelize");
let sequelize = require('../db/dbConfig');
const User = sequelize.define('user', {
    username: {
        type: Sequelize.STRING,
        unique: true,
        lowercase: true,
        required: true,
        trim: true,
    },
    firstname: {
        type: Sequelize.STRING,
        required: true
    },
    lastname: {
        type: Sequelize.STRING,
        required: true
    },
    email: {
        type: Sequelize.STRING,
        required: true,
    },

    pass: {
        type: Sequelize.STRING,
        required: true,
    }
});

module.exports = User;