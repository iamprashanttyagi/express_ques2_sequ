let bodyParser = require('body-parser');
let express = require('express');
let app = express();

let index = require('./routes/index');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));

app.use('/user', index);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

module.exports = app;