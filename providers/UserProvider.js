let register = (function (body) {
    return new Promise((resolve, reject) => {
        let username = body.username;
        let firstname = body.firstname;
        let lastname = body.lastname;
        let email = body.email;
        let pass = body.pass;
        let conpass = body.conpass;

        if (username == '' || username == null) {
            reject("Plz, enter the value of username")
        } else if (firstname == "" || firstname == null) {
            reject("Plz, enter the value of firstname")
        } else if (lastname == "" || lastname == null) {
            reject("Plz, enter the value of lastname")
        } else if (email == "" || email == null) {
            reject("Plz, enter the value of email")
        } else if (pass == "" || pass == null) {
            reject("Plz, enter the value of pass")
        } else if (pass !== conpass) {
            reject("Password is match")
        } else {
            resolve(body);
        }
    });
});
module.exports = register;