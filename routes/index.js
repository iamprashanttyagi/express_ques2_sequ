let express = require('express');
let router = express.Router();
let user = require("../controllers/User");

router.post('/register', user.register);

module.exports = router;